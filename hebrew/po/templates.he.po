# path to file ../hebrew/po
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml templates\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2008-09-30 11:22+0100\n"
"Last-Translator: Oz Nahum <nahumoz@gmail.com>\n"
"Language-Team: Hebrew <he@li.org>\n"
"Language: he\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/search.xml.in:7
#, fuzzy
msgid "Debian website"
msgstr "פרויקט דביאן"

#: ../../english/search.xml.in:9
msgid "Search the Debian website."
msgstr ""

#: ../../english/template/debian/basic.wml:19
#: ../../english/template/debian/navbar.wml:11
#, fuzzy
msgid "Debian"
msgstr "עזרו לדביאן"

#: ../../english/template/debian/basic.wml:48
msgid "Debian website search"
msgstr ""

#: ../../english/template/debian/common_translation.wml:4
msgid "Yes"
msgstr "כן"

#: ../../english/template/debian/common_translation.wml:7
msgid "No"
msgstr "לא"

#: ../../english/template/debian/common_translation.wml:10
msgid "Debian Project"
msgstr "פרויקט דביאן"

#: ../../english/template/debian/common_translation.wml:13
#, fuzzy
msgid ""
"Debian is an operating system and a distribution of Free Software. It is "
"maintained and updated through the work of many users who volunteer their "
"time and effort."
msgstr ""
"דביאן גנו/לינוקס היא הפצה חופשית של מערכת ההפעלה גנו/לינוקס. פרויקט זה "
"מתחוזק ומעודכן על ידי עבודתם של משתמשים רבים התורמים מזמנם ומרצם בזמנם "
"החופשי. "

#: ../../english/template/debian/common_translation.wml:16
msgid "debian, GNU, linux, unix, open source, free, DFSG"
msgstr "דביאן, גנו, לינוקס, יוניקס, תוכנה חופשית, חופשי, DFSG"

#: ../../english/template/debian/common_translation.wml:19
msgid "Back to the <a href=\"m4_HOME/\">Debian Project homepage</a>."
msgstr "חזרה לעמוד הבית של  <a href=\"m4_HOME\">פרויקט דביאן</a>."

#: ../../english/template/debian/common_translation.wml:22
#: ../../english/template/debian/links.tags.wml:149
msgid "Home"
msgstr "עמוד הבית"

#: ../../english/template/debian/common_translation.wml:25
msgid "Skip Quicknav"
msgstr "דלג על חיפוש מהיר"

#: ../../english/template/debian/common_translation.wml:28
msgid "About"
msgstr "אודות"

#: ../../english/template/debian/common_translation.wml:31
msgid "About Debian"
msgstr "אודות דביאן"

#: ../../english/template/debian/common_translation.wml:34
msgid "Contact Us"
msgstr "צור  קשר"

#: ../../english/template/debian/common_translation.wml:37
#, fuzzy
msgid "Legal Info"
msgstr "מידע על הפצות"

#: ../../english/template/debian/common_translation.wml:40
msgid "Data Privacy"
msgstr ""

#: ../../english/template/debian/common_translation.wml:43
msgid "Donations"
msgstr "תרומות"

#: ../../english/template/debian/common_translation.wml:46
msgid "Events"
msgstr "אירועים"

#: ../../english/template/debian/common_translation.wml:49
msgid "News"
msgstr "חדשות"

#: ../../english/template/debian/common_translation.wml:52
msgid "Distribution"
msgstr "הפצה"

#: ../../english/template/debian/common_translation.wml:55
msgid "Support"
msgstr "תמיכה"

#: ../../english/template/debian/common_translation.wml:58
msgid "Pure Blends"
msgstr ""

#: ../../english/template/debian/common_translation.wml:61
#: ../../english/template/debian/links.tags.wml:46
msgid "Developers' Corner"
msgstr "רוצה לתרום ?"

#: ../../english/template/debian/common_translation.wml:64
msgid "Documentation"
msgstr "תיעוד"

#: ../../english/template/debian/common_translation.wml:67
msgid "Security Information"
msgstr "הודעות אבטחה "

#: ../../english/template/debian/common_translation.wml:70
msgid "Search"
msgstr "חיפוש"

#: ../../english/template/debian/common_translation.wml:73
msgid "none"
msgstr "אין"

#: ../../english/template/debian/common_translation.wml:76
msgid "Go"
msgstr "טען"

#: ../../english/template/debian/common_translation.wml:79
msgid "worldwide"
msgstr "ברחבי העולם"

#: ../../english/template/debian/common_translation.wml:82
msgid "Site map"
msgstr "מפת אתר"

#: ../../english/template/debian/common_translation.wml:85
msgid "Miscellaneous"
msgstr "שונות"

#: ../../english/template/debian/common_translation.wml:88
#: ../../english/template/debian/links.tags.wml:104
msgid "Getting Debian"
msgstr "הורדה"

#: ../../english/template/debian/common_translation.wml:91
#, fuzzy
msgid "The Debian Blog"
msgstr "ספרים על דביאן"

#: ../../english/template/debian/common_translation.wml:94
#, fuzzy
#| msgid "Debian Project News"
msgid "Debian Micronews"
msgstr "חדשות פרויקט דביאן"

#: ../../english/template/debian/common_translation.wml:97
#, fuzzy
#| msgid "Debian Project"
msgid "Debian Planet"
msgstr "פרויקט דביאן"

#: ../../english/template/debian/common_translation.wml:100
#, fuzzy
msgid "Last Updated"
msgstr "עודכן לאחרונה"

#: ../../english/template/debian/ddp.wml:6
msgid ""
"Please send all comments, criticisms and suggestions about these web pages "
"to our <a href=\"mailto:debian-doc@lists.debian.org\">mailing list</a>."
msgstr ""
"נא שלחו את כל ההערות, ביקורות והצעות בנוגע לדפים אלו אל <a href=\"mailto:"
"debian-doc@lists.debian.org\">רשימת התפוצה </a>."

#: ../../english/template/debian/fixes_link.wml:11
msgid "not needed"
msgstr "לא צריך"

#: ../../english/template/debian/fixes_link.wml:14
msgid "not available"
msgstr "אינו זמין"

#: ../../english/template/debian/fixes_link.wml:17
msgid "N/A"
msgstr "אינו זמין"

#: ../../english/template/debian/fixes_link.wml:20
msgid "in release 1.1"
msgstr "בגרסה 1.1"

#: ../../english/template/debian/fixes_link.wml:23
msgid "in release 1.3"
msgstr "בגרסה 1.3"

#: ../../english/template/debian/fixes_link.wml:26
msgid "in release 2.0"
msgstr "בגרסה 2.0"

#: ../../english/template/debian/fixes_link.wml:29
msgid "in release 2.1"
msgstr "בגרסה 2.1"

#: ../../english/template/debian/fixes_link.wml:32
msgid "in release 2.2"
msgstr "בגרסה 2.2"

#. TRANSLATORS: Please make clear in the translation of the following
#. item that mail sent to the debian-www list *must* be in English. Also,
#. you can add some information of your own translation mailing list
#. (i.e. debian-l10n-XXXXXX@lists.debian.org) for reporting things in
#. your language.
#: ../../english/template/debian/footer.wml:89
#, fuzzy
msgid ""
"To report a problem with the web site, please e-mail our publicly archived "
"mailing list <a href=\"mailto:debian-www@lists.debian.org\">debian-www@lists."
"debian.org</a> in English.  For other contact information, see the Debian <a "
"href=\"m4_HOME/contact\">contact page</a>. Web site source code is <a href="
"\"https://salsa.debian.org/webmaster-team/webwml\">available</a>."
msgstr ""
"על מנת לדווח על בעיות באתר, שלחו דואר אלקטרוני ל<a href=\"mailto:debian-"
"www@lists.debian.org\">debian-www@lists.debian.org</a>. למידע נוסף על אפשרות "
"יצירת קשר, בקרו ב<a href=\"m4_HOME/contact\">דף הקשר</a> של דביאן."

#: ../../english/template/debian/footer.wml:92
msgid "Last Modified"
msgstr "עודכן לאחרונה"

#: ../../english/template/debian/footer.wml:95
msgid "Last Built"
msgstr ""

#: ../../english/template/debian/footer.wml:98
msgid "Copyright"
msgstr "כל הזכויות שמורות"

#: ../../english/template/debian/footer.wml:101
msgid "<a href=\"https://www.spi-inc.org/\">SPI</a> and others;"
msgstr ""

#: ../../english/template/debian/footer.wml:104
msgid "See <a href=\"m4_HOME/license\" rel=\"copyright\">license terms</a>"
msgstr "ראו <a href=\"m4_HOME/license\" rel=\"copyright\">תנאי הרשיון</a>"

#: ../../english/template/debian/footer.wml:107
msgid ""
"Debian is a registered <a href=\"m4_HOME/trademark\">trademark</a> of "
"Software in the Public Interest, Inc."
msgstr ""
"השם דביאן הינו <a href=\"m4_HOME/trademark\">סימן מסחרי רשום</a> של תאגיד "
"רשום 'תוכנה בשירות הציבור' (Software in the Public Interest, Inc)."

#: ../../english/template/debian/languages.wml:196
#: ../../english/template/debian/languages.wml:232
msgid "This page is also available in the following languages:"
msgstr "עמוד זה זמין גם בשפות הבאות:"

#: ../../english/template/debian/languages.wml:265
msgid "How to set <a href=m4_HOME/intro/cn>the default document language</a>"
msgstr ""
"הוראות כיצד לקבוע <a href=m4_HOME/intro/cn>את שפת ברירת המחדל</a> של דף זה."

#: ../../english/template/debian/languages.wml:323
msgid "Browser default"
msgstr ""

#: ../../english/template/debian/languages.wml:323
msgid "Unset the language override cookie"
msgstr ""

#: ../../english/template/debian/links.tags.wml:4
msgid "Debian International"
msgstr "דביאן ברחבי העולם"

#: ../../english/template/debian/links.tags.wml:7
msgid "Partners"
msgstr "שותפים"

#: ../../english/template/debian/links.tags.wml:10
msgid "Debian Weekly News"
msgstr "חדשות דביאן שבועיות"

#: ../../english/template/debian/links.tags.wml:13
msgid "Weekly News"
msgstr "שבועיות חדשות "

#: ../../english/template/debian/links.tags.wml:16
msgid "Debian Project News"
msgstr "חדשות פרויקט דביאן"

#: ../../english/template/debian/links.tags.wml:19
msgid "Project News"
msgstr "חדשות הפרויקט"

#: ../../english/template/debian/links.tags.wml:22
msgid "Release Info"
msgstr "מידע על הפצות"

#: ../../english/template/debian/links.tags.wml:25
msgid "Debian Packages"
msgstr "חבילות דביאן"

#: ../../english/template/debian/links.tags.wml:28
msgid "Download"
msgstr "הורדה"

#: ../../english/template/debian/links.tags.wml:31
msgid "Debian&nbsp;on&nbsp;CD"
msgstr "דביאן על תקליטורים"

#: ../../english/template/debian/links.tags.wml:34
msgid "Debian Books"
msgstr "ספרים על דביאן"

#: ../../english/template/debian/links.tags.wml:37
#, fuzzy
msgid "Debian Wiki"
msgstr "עזרו לדביאן"

#: ../../english/template/debian/links.tags.wml:40
msgid "Mailing List Archives"
msgstr "ארכיון רשימות תפוצה"

#: ../../english/template/debian/links.tags.wml:43
msgid "Mailing Lists"
msgstr "רשימות תפוצה"

#: ../../english/template/debian/links.tags.wml:49
msgid "Social Contract"
msgstr "חוזה חברתי"

#: ../../english/template/debian/links.tags.wml:52
msgid "Code of Conduct"
msgstr ""

#: ../../english/template/debian/links.tags.wml:55
msgid "Debian 5.0 - The universal operating system"
msgstr ""

#: ../../english/template/debian/links.tags.wml:58
msgid "Site map for Debian web pages"
msgstr "מפת אתר עבור דפי הרשת של דביאן"

#: ../../english/template/debian/links.tags.wml:61
msgid "Developer Database"
msgstr "מאגר מידע על המפתחים"

#: ../../english/template/debian/links.tags.wml:64
#, fuzzy
msgid "Debian FAQ"
msgstr "ספרים על דביאן"

#: ../../english/template/debian/links.tags.wml:67
msgid "Debian Policy Manual"
msgstr "מדריך מדיניות דביאן"

#: ../../english/template/debian/links.tags.wml:70
msgid "Developers' Reference"
msgstr "מידע למפתחים"

#: ../../english/template/debian/links.tags.wml:73
msgid "New Maintainers' Guide"
msgstr "מדריך למתחזקים חדשים"

#: ../../english/template/debian/links.tags.wml:76
msgid "Release Critical Bugs"
msgstr "באגים קריטיים לשחרור"

#: ../../english/template/debian/links.tags.wml:79
msgid "Lintian Reports"
msgstr ""

#: ../../english/template/debian/links.tags.wml:83
msgid "Archives for users' mailing lists"
msgstr "ארכיונים של רשימות התפוצה למשתמשים"

#: ../../english/template/debian/links.tags.wml:86
msgid "Archives for developers' mailing lists"
msgstr "ארכיונים של רשימות התפוצה למפתחים"

#: ../../english/template/debian/links.tags.wml:89
msgid "Archives for i18n/l10n mailing lists"
msgstr "ארכיונים של רשמיות התפוצה למתרגמים"

#: ../../english/template/debian/links.tags.wml:92
msgid "Archives for ports' mailing lists"
msgstr "ארכיונים של רשימות התפוצה לארכיטקטורות חומרה "

#: ../../english/template/debian/links.tags.wml:95
msgid "Archives for mailing lists of the Bug tracking system"
msgstr "ארכיון רשימות תפוצה של מערכת מעקב באגים"

#: ../../english/template/debian/links.tags.wml:98
msgid "Archives for miscellaneous mailing lists"
msgstr "ארכיונים של רשימות תפוצה שונות"

#: ../../english/template/debian/links.tags.wml:101
msgid "Free Software"
msgstr "תוכנה חופשית"

#: ../../english/template/debian/links.tags.wml:107
msgid "Development"
msgstr "פיתוח"

#: ../../english/template/debian/links.tags.wml:110
msgid "Help Debian"
msgstr "עזרו לדביאן"

#: ../../english/template/debian/links.tags.wml:113
msgid "Bug reports"
msgstr "דיווח באגים"

#: ../../english/template/debian/links.tags.wml:116
msgid "Ports/Architectures"
msgstr "ארכיטקטורות חומרה"

#: ../../english/template/debian/links.tags.wml:119
msgid "Installation manual"
msgstr "מדריך התקנה"

#: ../../english/template/debian/links.tags.wml:122
msgid "CD vendors"
msgstr "ספקי תקליטורים"

#: ../../english/template/debian/links.tags.wml:125
#, fuzzy
msgid "CD/USB ISO images"
msgstr "מדיה לצריבה על תקליטורים"

#: ../../english/template/debian/links.tags.wml:128
msgid "Network install"
msgstr "התקנת רשת"

#: ../../english/template/debian/links.tags.wml:131
msgid "Pre-installed"
msgstr "מותקן מראש"

#: ../../english/template/debian/links.tags.wml:134
msgid "Debian-Edu project"
msgstr "פרויקט דביאן בחינוך"

#: ../../english/template/debian/links.tags.wml:137
msgid "Alioth &ndash; Debian GForge"
msgstr ""

#: ../../english/template/debian/links.tags.wml:140
msgid "Quality Assurance"
msgstr "אבטחת איכות"

#: ../../english/template/debian/links.tags.wml:143
msgid "Package Tracking System"
msgstr "מערכת מעקב חבילות"

#: ../../english/template/debian/links.tags.wml:146
msgid "Debian Developer's Packages Overview"
msgstr ""

#: ../../english/template/debian/navbar.wml:10
#, fuzzy
msgid "Debian Home"
msgstr "פרויקט דביאן"

#: ../../english/template/debian/recent_list.wml:7
msgid "No items for this year."
msgstr "אין פריטים לשנה זו"

#: ../../english/template/debian/recent_list.wml:11
msgid "proposed"
msgstr "מוצע"

#: ../../english/template/debian/recent_list.wml:15
msgid "in discussion"
msgstr "בדיון"

#: ../../english/template/debian/recent_list.wml:19
msgid "voting open"
msgstr "ההצבעה פתוחה"

#: ../../english/template/debian/recent_list.wml:23
msgid "finished"
msgstr "הסתיימה"

#: ../../english/template/debian/recent_list.wml:26
msgid "withdrawn"
msgstr "הוסר מדיון"

#: ../../english/template/debian/recent_list.wml:30
msgid "Future events"
msgstr "אירועים עתידיים"

#: ../../english/template/debian/recent_list.wml:33
msgid "Past events"
msgstr "אירועי עבר"

#: ../../english/template/debian/recent_list.wml:37
msgid "(new revision)"
msgstr "(עדכון חדש)"

#: ../../english/template/debian/recent_list.wml:329
msgid "Report"
msgstr "דוח"

#: ../../english/template/debian/redirect.wml:6
msgid "Page redirected to <newpage/>"
msgstr ""

#: ../../english/template/debian/redirect.wml:12
msgid ""
"This page has been renamed to <url <newpage/>>, please update your links."
msgstr ""

#. given a manual name and an architecture, join them
#. if you need to reorder the two, use "%2$s ... %1$s", cf. printf(3)
#: ../../english/template/debian/release.wml:7
msgid "<void id=\"doc_for_arch\" />%s for %s"
msgstr ""

#: ../../english/template/debian/translation-check.wml:37
msgid ""
"<em>Note:</em> The <a href=\"$link\">original document</a> is newer than "
"this translation."
msgstr ""

#: ../../english/template/debian/translation-check.wml:43
msgid ""
"Warning! This translation is too out of date, please see the <a href=\"$link"
"\">original</a>."
msgstr ""

#: ../../english/template/debian/translation-check.wml:49
msgid ""
"<em>Note:</em> The original document of this translation no longer exists."
msgstr ""

#: ../../english/template/debian/translation-check.wml:56
msgid "Wrong translation version!"
msgstr ""

#: ../../english/template/debian/url.wml:4
msgid "URL"
msgstr ""

#: ../../english/template/debian/users.wml:12
msgid "Back to the <a href=\"../\">Who's using Debian? page</a>."
msgstr "חזרה אל עמוד  <a href=\"../\">מי משתמש בדביאן?</a>."

#~ msgid "Rating:"
#~ msgstr "דירוג:"

#~ msgid "Nobody"
#~ msgstr "אף אחד"

#~ msgid "Taken by:"
#~ msgstr "נלקח על ידי:"

#~ msgid "More information:"
#~ msgstr "מידע נוסף:"

#~ msgid "Select a server near you"
#~ msgstr "בחר שרת קרוב אליך"

#~ msgid "Visit the site sponsor"
#~ msgstr "בקרו באתר נותן החסות"
