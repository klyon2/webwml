#use wml::debian::template title="Debian 衍生發行版"
#use wml::debian::translation-check translation="1b62dcf348569f7b3d0975c674a776e714dc31fc"

<p>
有<a href="#list">许多基于 Debian 的发行版</a>。有些人可能还想看看这些 Debian 官方发行版\
<em>之外</em>的发行版。
</p>

<p>
一个 Debian 衍生發行版是一种基于 Debian 所做工作的发行版，但是具有自己的独特标识，目标和\
受众，并且由独立于 Debian 的实体创建。衍生發行版会修改 Debian 来实现自己设定的目标。
</p>

<p>
Debian 欢迎并鼓励想要基于 Debian 来开发新发行版的组织。本着 Debian 的\
<a href="$(HOME)/social_contract">社群契约</a>精神，我们希望这些衍生發行版能为 Debian 与\
上游项目贡献他们的工作，以便每个人都可以从这些改进中受益。
</p>

<h2 id="list">有哪些衍生發行版？</h2>

<p>
我们要突出显示以下的 Debian 衍生發行版：
</p>

## Please keep this list sorted alphabetically
## Please only add derivatives that meet the criteria below
<ul>
    <li>
      <a href="https://grml.org/">Grml</a>:
      面向系统管理员的 live 系统。
      <a href="https://wiki.debian.org/Derivatives/Census/Grml">更多信息</a>.
    </li>
    <li>
      <a href="https://www.kali.org/">Kali Linux</a>:
      安全审计和渗透测试。
      <a href="https://wiki.debian.org/Derivatives/Census/Kali">更多信息</a>.
    </li>
    <li>
      <a href="https://pureos.net/">Purism PureOS</a>:
	  <a href="https://www.fsf.org/news/fsf-adds-pureos-to-list-of-endorsed-gnu-linux-distributions-1">获得 FSF 认可的</a>
	  滚动发行版，专注于隐私、安全和便捷。
      <a href="https://wiki.debian.org/Derivatives/Census/Purism">更多信息</a>.
    </li>
    <li>
      <a href="https://tails.boum.org/">Tails</a>:
      保护隐私和保持匿名。
      <a href="https://wiki.debian.org/Derivatives/Census/Tails">更多信息</a>.
    </li>
    <li>
      <a href="https://www.ubuntu.com/">Ubuntu</a>:
      在世界范围内推广 Linux。
      <a href="https://wiki.debian.org/Derivatives/Census/Ubuntu">更多信息</a>.
    </li>
</ul>

<p>
此外，在<a href="https://wiki.debian.org/Derivatives/Census">Debian 衍生發行版普查</a>\
以及<a href="https://wiki.debian.org/Derivatives#Lists">其它地方</a>也列出了基于 \
Debian 的发行版 。
</p>

<h2>为什么用一个衍生發行版代替 Debian？</h2>

<p>
如果您有特定需求，而衍生發行版可以更好地满足您的需求，那么您可能更喜欢使用它而不是 Debian。
</p>

<p>
如果您是某个特定社区或特定人群的成员，并且存在一个为该人群准备的衍生發行版，那么您可能更\
喜欢使用它而不是 Debian。
</p>

<h2>为什么 Debian 对其衍生發行版感兴趣？</h2>

<p>
衍生發行版将 Debian 带给了大量与我们目前的受众群体相比具有更多经验和要求的人。通过建立与\
衍生發行版的联系，将有关它们的信息
<a href="https://wiki.debian.org/Derivatives/Integration">整合</a>到 Debian 的基础设施中，\
并将它们所做的更改合并回 Debian，我们与衍生發行版分享我们的经验，扩展我们对衍生發行版及其\
受众的了解，潜在地扩展 Debian 社区，为我们现有的受众群体改进 Debian，使 Debian 更适合更多样\
化的受众群体。
</p>

<h2>Debian 将突出显示哪些衍生發行版？</h2>

## Examples of these criteria are in the accompanying README.txt
<p>
上面突出显示的衍生發行版均满足大多数下面列举的条件：
</p>

<ul>
    <li>积极地与 Debian 合作</li>
    <li>在活跃维护状态</li>
    <li>有一组起码包括一个 Debian 成员的人员参与</li>
    <li>加入了 Debian 衍生發行版普查，并在其普查页面中包含一个 sources.list</li>
    <li>有特色或重点目标</li>
    <li>是知名且历史悠久的发行版</li>
</ul>

<h2>为什么衍生自 Debian？</h2>

<p>
修改现有的发行版（例如 Debian）比从头开始要快，因为打包格式、存储库、基础软件包和其它内容\
已指定且可用。大量软件已被打包，因此衍生發行版无需花费时间打包大多数东西，这使衍生發行版\
可以专注于特定受众的需求。
</p>

<p>
Debian 确保我们分发的内容对于衍生發行版而言是能<a href="$(HOME)/intro/free">自由</a>修改并\
重新分发给他们的受众的。我们通过检查我们分发的软件的许可证是否符合 
<a href="$(HOME)/social_contract#guidelines">Debian 自由软件指导方针 (DFSG)</a>来做到这\
一点。
</p>

<p>
Debian 有许多衍生發行版可用以构建他们的发行版的不同的<a href="$(HOME)/releases/">发行</a>\
周期。这使衍生發行版可以
尝试<a href="https://wiki.debian.org/DebianExperimental">实验性</a>软件、\
<a href="$(HOME)/releases/unstable/">飞快</a>地更新、\
<a href="$(HOME)/releases/testing/">经常</a>获得有质量保证的更新、\
为他们的工作获得<a href="$(HOME)/releases/stable/">坚实的基础</a>、\
在坚实的基础之上使用<a href="https://backports.debian.org/">更新的</a>软件、\
享受<a href="$(HOME)/security/">安全</a>支持并\
<a href="https://wiki.debian.org/LTS">扩展</a>该支持。
</p>

<p>
Debian 支持许多不同的<a href="$(HOME)/ports/">体系架构</a>，并且贡献者们正在\
<a href="https://wiki.debian.org/DebianBootstrap">研究</a>为新种类的处理器自动创建新的体系\
架构支持的方法。这令衍生發行版可以使用其选择的硬件或支持新的处理器。
</p>

<p>
Debian 社区和现有的衍生發行版的成员都可以享受上述好处，并且他们乐意帮忙指导新发行版的这\
部分工作。
</p>

<p>
存在很多原因使得衍生發行版被创建，例如需要翻译成新语言、特定的硬件支持、不同的安装机制或者\
为特定的社区或一群人提供支持。
</p>

<h2>如何衍生自 Debian？</h2>

<p>
如果需要的话，衍生發行版可以使用一部分 Debian 基础设施（例如存储库）。衍生發行版应更改对 \
Debian（如 logo，名称等）和 Debian 服务（如网站和 BTS）的引用。
</p>

<p>
如果您的目标是定义一组预先安装的软件包，那么在 Debian 中创建一个新的 \
<a href="$(HOME)/blends/">Debian blend</a> 可能是一个比较有趣的方式。
</p>

<p>
<a href="https://wiki.debian.org/Derivatives/Guidelines">衍生指南</a>提供了详细的开发信息，\
<a href="https://wiki.debian.org/DerivativesFrontDesk">前台</a>提供了相关指南。
</p>
