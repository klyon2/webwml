<define-tag pagetitle>Debian 10 aktualisiert: 10.10 veröffentlicht</define-tag>
<define-tag release_date>2021-06-19</define-tag>
#use wml::debian::news
#use wml::debian::translation-check translation="e381a4552e96e017684cf783d08d728d9ad833af" maintainer="Erik Pfannenstein"
# $Id:

<define-tag release>10</define-tag>
<define-tag codename>Buster</define-tag>
<define-tag revision>10.10</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>
Das Debian-Projekt freut sich, die zehnte Aktualisierung seiner 
Stable-Veröffentlichung Debian <release> (Codename <q><codename></q>) 
ankündigen zu dürfen. Diese Aktualisierung bringt hauptsächlich Korrekturen für
Sicherheitsprobleme und Anpassungen für einige ernste Probleme. Für sie sind
bereits separate Sicherheitsankündigungen veröffentlicht worden; auf diese
wird, wo möglich, verwiesen.
</p>

<p>Bitte beachten Sie, dass diese Zwischenveröffentlichung keine neue Version
von Debian <release> darstellt, sondern nur einige der enthaltenen Pakete
auffrischt. Es gibt keinen Grund, <q><codename></q>-Medien zu entsorgen, da
deren Pakete nach der Installation mit Hilfe eines aktuellen
Debian-Spiegelservers auf den neuesten Stand gebracht werden können.</p>

<p>Wer häufig Aktualisierungen von security.debian.org herunterlädt, wird nicht
viele Pakete auf den neuesten Stand bringen müssen. Die meisten
Aktualisierungen sind in dieser Revision enthalten.</p>

<p>Neue Installationsabbilder können bald von den gewohnten Orten bezogen werden.</p>

<p>Vorhandene Installationen können auf diese Revision angehoben werden, indem
das Paketverwaltungssystem auf einen der vielen HTTP-Spiegel von Debian
verwiesen wird. Eine vollständige Liste der Spiegelserver ist verfügbar unter:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>

<h2>Verschiedene Fehlerkorrekturen</h2>

<p>Diese Stable-Veröffentlichung nimmt an den folgenden Paketen einige wichtige
Korrekturen vor:</p>

<table border=0>
<tr><th>Paket</th>               <th>Grund</th></tr>
<correction apt "Änderungen im Suite-Namen der Paketdepots standardmäßig akzeptieren (z. B. stable -&gt; oldstable)">
<correction awstats "Probleme mit Dateizugriff aus der Ferne behoben [CVE-2020-29600 CVE-2020-35176]">
<correction base-files "/etc/debian_version auf die Zwischenveröffentlichung 10.10 aktualisiert">
<correction berusky2 "Speicherzugriffsfehler beim Programmstart behoben">
<correction clamav "Neue Veröffentlichung der Originalautoren; Sicherheitsblockade (Denial of security) behoben [CVE-2021-1405]">
<correction clevis "Unterstützung für TPMs, die nur SHA256 unterstützen, überarbeitet">
<correction connman "dnsproxy: vor dem memcpy die Länge der Puffer überprüfen [CVE-2021-33833]">
<correction crmsh "Problem mit Codeausführung behoben [CVE-2020-35459]">
<correction debian-installer "Linux-Kernel-ABI 4.19.0-17 verwenden">
<correction debian-installer-netboot-images "Neubau gegen proposed-updates">
<correction dnspython "XFR: nicht versuchen, Vergleiche mit einem nicht existenten <q>expiration</q> (Ablauf)-Wert anzustellen">
<correction dput-ng "Absturz im SFTP-Uploader behoben, der auftritt, wenn ein EACCES vom Server kommt; Codenamen aktualisiert; <q>dcut dm</q> für nicht-hochladende DDs zum Laufen bekommen; TypeError in der Ausnahme-Handhabung für HTTP-Uploads behoben; nicht versuchen, Uploader-E-Mail vom System-Hostnamen in .dak-commands-Dateien zu konstruieren">
<correction eterm "Problem mit Codeausführung behoben [CVE-2021-33477]">
<correction exactimage "Kompilierungsprobleme mit C++11 und OpenEXR 2.5.x behoben">
<correction fig2dev "Pufferüberlauf behoben [CVE-2021-3561]; mehrere Ausgabeprobleme behoben; Testsuite während Kompilierung und in autopkgtest neu kompilieren">
<correction fluidsynth "Use-after-free behoben [CVE-2021-28421]">
<correction freediameter "Dienstblockade behoben [CVE-2020-6098]">
<correction fwupd "Erzeugung der Hersteller-SBAT-Zeichenkette überarbeitet; dpkg-dev nicht mehr in fwupd.preinst verwenden; neue stabile Version der Originalautoren">
<correction fwupd-amd64-signed "Abgleich mit fwupd">
<correction fwupd-arm64-signed "Abgleich mit fwupd">
<correction fwupd-armhf-signed "Abgleich mit fwupd">
<correction fwupd-i386-signed "Abgleich mit fwupd">
<correction fwupdate "SBAT-Unterstützung verbessert">
<correction fwupdate-amd64-signed "Abgleich mit fwupdate">
<correction fwupdate-arm64-signed "Abgleich mit fwupdate">
<correction fwupdate-armhf-signed "Abgleich mit fwupdate">
<correction fwupdate-i386-signed "Abgleich mit fwupdate">
<correction glib2.0 "Mehrere Pufferüberläufe behoben [CVE-2021-27218 CVE-2021-27219]; Symlink-Angriff behoben, der file-roller betraf [CVE-2021-28153]">
<correction gnutls28 "Nullzeiger-Dereferenzierung behoben [CVE-2020-24659]; mehrere Verbesserungen bei Speicher-Wiederzuweisung behoben">
<correction golang-github-docker-docker-credential-helpers "Double Free behoben [CVE-2019-1020014]">
<correction htmldoc "Pufferüberläufe behoben [CVE-2019-19630 CVE-2021-20308]">
<correction ipmitool "Pufferüberläufe behoben [CVE-2020-5208]">
<correction ircii "Dienstblockade behoben [CVE-2021-29376]">
<correction isc-dhcp "Pufferüberlauf behoben [CVE-2021-25217]">
<correction isync "<q>Lustige</q> Mailbox-Namen in IMAP LIST/LSUB abweisen [CVE-2021-20247]; Umgang mit unerwartetem APPENDUID-Antwortcode überarbeitet [CVE-2021-3578]">
<correction jackson-databind "Problematische Erweiterung der externen Entität abgestellt [CVE-2020-25649] sowie mehrere Probleme rund um Serialisierung [CVE-2020-24616 CVE-2020-24750 CVE-2020-35490 CVE-2020-35491 CVE-2020-35728 CVE-2020-36179 CVE-2020-36180 CVE-2020-36181 CVE-2020-36182 CVE-2020-36183 CVE-2020-36184 CVE-2020-36185 CVE-2020-36186 CVE-2020-36187 CVE-2020-36188 CVE-2020-36189 CVE-2021-20190]">
<correction klibc "malloc: Bei Fehlschlag Fehlercode setzen; mehrere Überlaufprobleme behoben [CVE-2021-31873 CVE-2021-31870 CVE-2021-31872]; cpio: möglichen Absturz auf 64-Bit-Systemen verhindert [CVE-2021-31871]; {set,long}jmp [s390x]: die richtigen FPU-Register speichern/wiederherstellen">
<correction libbusiness-us-usps-webtools-perl "Aktualisierung auf neue US-USPS-API">
<correction libgcrypt20 "Schwache ElGamal-Verschlüsselung, bei der die Schlüssel nicht mit GnuPG/libgcrypt erzeugt wurden, überarbeitet [CVE-2021-33560]">
<correction libgetdata "Use-After-Free behoben [CVE-2021-20204]">
<correction libmateweather "Mit der Umbenennung von America/Godthab nach America/Nuuk in tzdata mitziehen">
<correction libxml2 "Lesezugriff außerhalb der Grenzen (out-of-bounds read) in xmllint behoben [CVE-2020-24977]; Use-after-Free in xmllint behoben [CVE-2021-3516 CVE-2021-3518]; UTF8 in xmlEncodeEntities validieren [CVE-2021-3517]; Fehler in xmlParseElementChildrenContentDeclPriv weiterreichen; Exponential-Entity-Expansion-Angriff verhindert [CVE-2021-3541]">
<correction liferea "Kompatibilität mit webkit2gtk &gt;= 2.32 verbessert">
<correction linux "Neue stabile Version der Originalautoren; ABI auf 17 anheben; [rt] Aktualisierung auf 4.19.193-rt81">
<correction linux-latest "Aktualisierung auf ABI 4.19.0-17">
<correction linux-signed-amd64 "Neue Version der Originalautoren; ABI auf 17 anheben; [rt] Aktualisierung auf 4.19.193-rt81">
<correction linux-signed-arm64 "Neue Version der Originalautoren; ABI auf 17 anheben; [rt] Aktualisierung auf 4.19.193-rt81">
<correction linux-signed-i386 "Neue Version der Originalautoren; ABI auf 17 anheben; [rt] Aktualisierung auf 4.19.193-rt81">
<correction mariadb-10.3 "Neue Version der Originalautoren; Sicherheitskorrekturen [CVE-2021-2154 CVE-2021-2166 CVE-2021-27928]; Innotop-Unterstützung überarbeitet; caching_sha2_password.so mitliefern">
<correction mqtt-client "Dienstblockade behoben [CVE-2019-0222]">
<correction mumble "Codeausführung aus der Ferne behoben [CVE-2021-27229]">
<correction mupdf "Use-After-Free [CVE-2020-16600] and Double-Free behoben [CVE-2021-3407]">
<correction nmap "Enthaltene MAC-Präfix-Liste aktualisiert">
<correction node-glob-parent "Dienstblockade mit regulären Ausdrücken behoben [CVE-2020-28469]">
<correction node-handlebars "Codeausführung behoben [CVE-2019-20920 CVE-2021-23369]">
<correction node-hosted-git-info "Dienstblockade mit regulären Ausdrücken behoben [CVE-2021-23362]">
<correction node-redis "Dienstblockade mit regulären Ausdrücken behoben [CVE-2021-29469]">
<correction node-ws "Dienstblockade mit Bezug auf reguläre Ausdrücke behoben [CVE-2021-32640]">
<correction nvidia-graphics-drivers "Anfälligkeit für unzureichende Zugriffskontrolle behoben [CVE-2021-1076]">
<correction nvidia-graphics-drivers-legacy-390xx "Anfälligkeit für unzureichende Zugriffskontrolle behoben [CVE-2021-1076]; Installationsfehlschlag unter Linux-5.11-Veröffentlichungskandidaten behoben">
<correction opendmarc "Heap-Overflow behoben [CVE-2020-12460]">
<correction openvpn "<q>illegal client float</q> (Sitzungsdiebstahl mittels <q>client float</q>-Mechanismus) behoben [CVE-2020-11810]; sicherstellen, dass die Schlüssel den Authentifiziert-Status haben, bevor die Push-Antwort gesendet wird [CVE-2020-15078]; listen()-Backlog-Warteschlange auf 32 erhöht">
<correction php-horde-text-filter "Seitenübergreifendes Skripting behoben [CVE-2021-26929]">
<correction plinth "Sitzung verwenden, um den First-Boot-Willkommen-Schritt zu verifizieren">
<correction ruby-websocket-extensions "Dienstblockade behoben [CVE-2020-7663]">
<correction rust-rustyline "Kompilierungsprobleme mit neuerem rustc korrigiert">
<correction rxvt-unicode "Escape-Sequenz ESC G Q deaktiviert [CVE-2021-33477]">
<correction sabnzbdplus "Anfälligkeit für Codeausführung beseitigt [CVE-2020-13124]">
<correction scrollz "Dienstblockade behoben [CVE-2021-29376]">
<correction shim "Neue Version der Originalautoren; SBAT-Unterstützung hinzugefügt; i386-Binär-Relokationen überarbeitet; QueryVariableInfo() auf Maschinen mit EFI 1.10 (wie älteren Intel-Macs) nicht aufrufen; Umgang mit ignore_db und user_insecure_mode korrigiert; Betreuerskripte zu den Vorlagepaketen hinzufügen, damit die sich um die Installation und Entfernung von fbXXX.efi und mmXXX.efi kümmern, wenn wir die shim-helpers-$arch-signed-Pakete installieren/deinstallieren; bei Installation auf einem Nicht-EFI-System sauber beenden; nicht fehlschlagen, wenn debconf-Aufrufe Fehler zurückliefern">
<correction shim-helpers-amd64-signed "Abgleich mit shim">
<correction shim-helpers-arm64-signed "Abgleich mit shim">
<correction shim-helpers-i386-signed "Abgleich mit shim">
<correction shim-signed "Aktualisierung auf neues shim; mehere Fehler in postinst- und postrm-Handhabung behoben; unsignierte Binärdateien für arm64 anbieten (see NEWS.Debian); bei Installation auf einem Nicht-EFI-Systemen sauber beenden; nicht fehschlagen, wenn debconf-Aufrufe Fehler zurückliefern; Dokumentationslinks korrigiert; Kompilierung gegen shim-unsigned 15.4-5~deb10u1; explizite Abhängigkeit von shim-signed bei shim-signed-common hinzufügen">
<correction speedtest-cli "Den Fall berücksichtigen, dass <q>ignoreids</q> leer ist oder leere IDs enthält">
<correction tnef "Probleme mit übermäßigem Puffer-Auslesen (buffer over-read) behoben [CVE-2019-18849]">
<correction uim "libuim-data: <q>Beschädigt</q> von uim-data kopieren, um einige Upgrade-Szenarien hinzubekommen">
<correction user-mode-linux "Neukompilierung gegen Linux-Kernel 4.19.194-1">
<correction velocity "Potenzielle eigenmächtige Codeausführung behoben [CVE-2020-13936]">
<correction wml "Rückschritt im Umgang mit Unicode behoben">
<correction xfce4-weather-plugin "Umstellung auf Version 2.0 der met.no-API">
</table>

<h2>Sicherheitsaktualisierungen</h2>

<p>Diese Revision fügt der Stable-Veröffentlichung die folgenden
Sicherheitsaktualisierungen hinzu. Das Sicherheitsteam hat bereits für jede
davon eine Sicherheitsankündigung veröffentlicht.</p>

<table border=0>
<tr><th>Ankündigungs-ID</th>  <th>Paket</th></tr>
<dsa 2021 4848 golang-1.11>
<dsa 2021 4865 docker.io>
<dsa 2021 4873 squid>
<dsa 2021 4874 firefox-esr>
<dsa 2021 4875 openssl>
<dsa 2021 4877 webkit2gtk>
<dsa 2021 4878 pygments>
<dsa 2021 4879 spamassassin>
<dsa 2021 4880 lxml>
<dsa 2021 4881 curl>
<dsa 2021 4882 openjpeg2>
<dsa 2021 4883 underscore>
<dsa 2021 4884 ldb>
<dsa 2021 4885 netty>
<dsa 2021 4886 chromium>
<dsa 2021 4887 lib3mf>
<dsa 2021 4888 xen>
<dsa 2021 4889 mediawiki>
<dsa 2021 4890 ruby-kramdown>
<dsa 2021 4891 tomcat9>
<dsa 2021 4892 python-bleach>
<dsa 2021 4893 xorg-server>
<dsa 2021 4894 php-pear>
<dsa 2021 4895 firefox-esr>
<dsa 2021 4896 wordpress>
<dsa 2021 4898 wpa>
<dsa 2021 4899 openjdk-11-jre-dcevm>
<dsa 2021 4899 openjdk-11>
<dsa 2021 4900 gst-plugins-good1.0>
<dsa 2021 4901 gst-libav1.0>
<dsa 2021 4902 gst-plugins-bad1.0>
<dsa 2021 4903 gst-plugins-base1.0>
<dsa 2021 4904 gst-plugins-ugly1.0>
<dsa 2021 4905 shibboleth-sp>
<dsa 2021 4907 composer>
<dsa 2021 4908 libhibernate3-java>
<dsa 2021 4909 bind9>
<dsa 2021 4910 libimage-exiftool-perl>
<dsa 2021 4912 exim4>
<dsa 2021 4913 hivex>
<dsa 2021 4914 graphviz>
<dsa 2021 4915 postgresql-11>
<dsa 2021 4916 prosody>
<dsa 2021 4918 ruby-rack-cors>
<dsa 2021 4919 lz4>
<dsa 2021 4920 libx11>
<dsa 2021 4921 nginx>
<dsa 2021 4922 hyperkitty>
<dsa 2021 4923 webkit2gtk>
<dsa 2021 4924 squid>
<dsa 2021 4925 firefox-esr>
<dsa 2021 4926 lasso>
<dsa 2021 4928 htmldoc>
<dsa 2021 4929 rails>
<dsa 2021 4930 libwebp>
</table>

<h2>Entfernte Pakete</h2>

<p>Die folgenden Pakete wurden wegen Umständen entfernt, die außerhalb unserer
Kontrolle liegen:</p>

<table border=0>
<tr><th>Paket</th>               <th>Grund</th></tr>
<correction sogo-connector "Mit den aktuellen Thunderbird-Versionen nicht kompatibel">
</table>

<h2>Debian-Installer</h2>
<p>Der Installer wurde aktualisiert, damit er die Sicherheitskorrekturen
enthält, die durch diese Zwischenveröffentlichung in Stable eingeflossen sind.</p>

<h2>URLs</h2>

<p>Die vollständige Liste von Paketen, die sich mit dieser Revision geändert haben:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Die derzeitige Stable-Veröffentlichung:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>Vorgeschlagene Aktualisierungen für die Stable-Distribution:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>Informationen zur Stable-Distribution (Veröffentlichungshinweise, Errata usw.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Sicherheitsankündigungen und -informationen:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Über Debian</h2>

<p>Das Debian-Projekt ist ein Zusammenschluss von Entwicklern Freier Software,
die ihre Zeit und Bemühungen einbringen, um das vollständig freie
Betriebssystem Debian zu erschaffen.</p>

<h2>Kontaktinformationen</h2>

<p>Für weitere Informationen besuchen Sie bitte die Debian-Website unter
<a href="$(HOME)/">https://www.debian.org/</a>, schicken eine E-Mail (auf
Englisch) an &lt;press@debian.org&gt; oder kontaktieren das Stable-Release-Team
(auch auf Englisch) unter &lt;debian-release@lists.debian.org&gt;.</p>
