#use wml::debian::translation-check translation="c1ea0c532237ebe87381e982acdfd0b88c70ad4f" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Nick Roessler et Rafi Rubin ont découvert que les analyseurs des
protocoles IMAP et ManageSieve dans le serveur de courrier électronique
Dovecot ne validaient pas correctement les entrées (à la fois avant et
après connexion). Un attaquant distant peut tirer avantage de ce défaut
pour déclencher des écritures de mémoire de tas hors limites, menant à des
fuites d'informations ou éventuellement à l'exécution de code arbitraire.</p>

<p>Pour la distribution oldstable (Stretch), ce problème a été corrigé dans
la version 1:2.2.27-3+deb9u5.</p>

<p>Pour la distribution stable (Buster), ce problème a été corrigé dans la
version 1:2.3.4.1-5+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets dovecot.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de dovecot, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/dovecot">\
https://security-tracker.debian.org/tracker/dovecot</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4510.data"
# $Id: $
