#use wml::debian::translation-check translation="603ec322379c4d19ac2617c6e9f713349947c37e" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le navigateur web
Chromium.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-16044">CVE-2020-16044</a>

<p>Ned Williamson a découvert un problème d'utilisation de mémoire après
libération dans l'implémentation de WebRTC.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21117">CVE-2021-21117</a>

<p>Rory McNamara a découvert un problème d'application de politique dans
Cryptohome.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21118">CVE-2021-21118</a>

<p>Tyler Nighswander a découvert un problème de validation de données dans
la bibliothèque JavaScript v8.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21119">CVE-2021-21119</a>

<p>Un problème d'utilisation de mémoire après libération a été découvert
dans le traitement des médias.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21120">CVE-2021-21120</a>

<p>Nan Wang et Guang Gong ont découvert un problème d'utilisation de
mémoire après libération dans l'implémentation de WebSQL.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21121">CVE-2021-21121</a>

<p>Leecraso et Guang Gong ont découvert un problème d'utilisation de
mémoire après libération dans l'Omnibox.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21122">CVE-2021-21122</a>

<p>Renata Hodovan a découvert un problème d'utilisation de mémoire après
libération dans Blink et WebKit.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21123">CVE-2021-21123</a>

<p>Maciej Pulikowski a découvert un problème de validation de données.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21124">CVE-2021-21124</a>

<p>Chaoyang Ding a découvert un problème d'utilisation de mémoire après
libération dans la reconnaissance vocale.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21125">CVE-2021-21125</a>

<p>Ron Masas a découvert un problème d'application de politique.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21126">CVE-2021-21126</a>

<p>David Erceg a découvert un problème d'application de politique dans les
extensions.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21127">CVE-2021-21127</a>

<p>Jasminder Pal Singh a découvert un problème d'application de politique
dans les extensions.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21128">CVE-2021-21128</a>

<p>Liang Dong a découvert un problème de dépassement de tampon dans Blink
et WebKit.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21129">CVE-2021-21129</a>

<p>Maciej Pulikowski a découvert un problème d'application de politique.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21130">CVE-2021-21130</a>

<p>Maciej Pulikowski a découvert un problème d'application de politique.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21131">CVE-2021-21131</a>

<p>Maciej Pulikowski a découvert un problème d'application de politique.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21132">CVE-2021-21132</a>

<p>David Erceg a découvert une erreur d'implémentation dans les outils de
développement.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21133">CVE-2021-21133</a>

<p>wester0x01 a découvert un problème d'application de politique.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21134">CVE-2021-21134</a>

<p>wester0x01 a découvert une erreur d'interface utilisateur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21135">CVE-2021-21135</a>

<p>ndevtk a découvert une erreur d'implémentation dans l'API Performance.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21136">CVE-2021-21136</a>

<p>Shiv Sahni, Movnavinothan V et Imdad Mohammed ont découvert une erreur
d'application de politique.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21137">CVE-2021-21137</a>

<p>bobbybear a découvert une erreur d'implémentation dans les outils de
développement.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21138">CVE-2021-21138</a>

<p>Weipeng Jiang a découvert un problème d'utilisation de mémoire après
libération dans les outils de développement.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21139">CVE-2021-21139</a>

<p>Jun Kokatsu a découvert une erreur d'implémentation dans le bac à sable
d'iframe.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21140">CVE-2021-21140</a>

<p>David Manouchehri a découvert un problème de mémoire non initialisée
dans l'implémentation d'USB.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21141">CVE-2021-21141</a>

<p>Maciej Pulikowski a découvert une erreur d'application de politique.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21142">CVE-2021-21142</a>

<p>Khalil Zhani a découvert un problème d'utilisation de mémoire après
libération.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21143">CVE-2021-21143</a>

<p>Allen Parker et Alex Morgan ont découvert un problème de dépassement de
tampon dans les extensions.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21144">CVE-2021-21144</a>

<p>Leecraso et Guang Gong ont découvert un problème de dépassement de
tampon.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21145">CVE-2021-21145</a>

<p>Un problème d'utilisation de mémoire après libération a été découvert.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21146">CVE-2021-21146</a>

<p>Alison Huffman et Choongwoo Han ont découvert un problème d'utilisation
de mémoire après utilisation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21147">CVE-2021-21147</a>

<p>Roman Starkov a découvert une erreur d'implémentation dans la
bibliothèque skia.</p></li>

</ul>

<p>Pour la distribution stable (Buster), ces problèmes ont été corrigés
dans la version 88.0.4324.146-1~deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets chromium.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de chromium, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/chromium">\
https://security-tracker.debian.org/tracker/chromium</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4846.data"
# $Id: $
