#use wml::debian::translation-check translation="5a92f5ba5c86bcac9b588a3e7656db8f48163007" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le noyau Linux qui
pourraient conduire à une élévation des privilèges, un déni de service ou
avoir d’autres impacts.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-9888">CVE-2014-9888</a>

<p>Russell King a trouvé que sur les systèmes ARM, la mémoire allouée pour les
tampons DMA était mappée avec la permission d’exécution. Cela facilitait
l’exploitation d’autres vulnérabilités dans le noyau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-9895">CVE-2014-9895</a>

<p>Dan Carpenter a trouvé que l’ioctl MEDIA_IOC_ENUM_LINKS sur des périphériques
de média aboutissait à une fuite d'informations.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6786">CVE-2016-6786</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2016-6787">CVE-2016-6787</a>

<p>Le sous-système « performance events » ne gère pas correctement les
verrouillages lors de certaines migrations, permettant à un attaquant local
d'augmenter ses droits. Cela peut être atténué en désactivant l'utilisation
non privilégiée de « performance events » :
sysctl kernel.perf_event_paranoid=3.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-8405">CVE-2016-8405</a>

<p>Peter Pi de Trend Micro a découvert que le sous-système de mémoire
d'image vidéo ne vérifie pas correctement les limites lors de la copie des
tables de couleurs dans l'espace utilisateur, provoquant une lecture hors
limites du tampon de tas, menant à la divulgation d'informations.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5549">CVE-2017-5549</a>

<p>Le pilote de périphérique USB série KLSI KL5KUSB105 pourrait enregistrer
le contenu de mémoire non initialisée du noyau avec pour conséquence une
fuite d'informations.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-6001">CVE-2017-6001</a>

<p>Di Shen a découvert une situation de compétition entre des appels
parallèles au sous-système « performance events », permettant à un attaquant
local d'augmenter ses droits. Ce défaut existe à cause d'une correction
incomplète de <a href="https://security-tracker.debian.org/tracker/CVE-2016-6786">CVE-2016-6786</a>.
Cela peut être atténué en désactivant l'utilisation non privilégiée de
« performance events » : sysctl kernel.perf_event_paranoid=3.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-6074">CVE-2017-6074</a>

<p>Andrey Konovalov a découvert une vulnérabilité d'utilisation de mémoire
après libération dans le code réseau DCCP qui pourrait avoir pour
conséquence un déni de service ou une augmentation des droits locale. Sur les
systèmes où le module dccp n'a pas déjà été chargé, cela peut être atténué
en le désactivant :
echo >> /etc/modprobe.d/disable-dccp.conf install dccp false.</p></li>
</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 3.2.84-2.</p>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 3.16.39-1+deb8u1 ou précédente.</p>

<p>Nous vous recommandons de mettre à jour vos paquets linux.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-833.data"
# $Id: $
