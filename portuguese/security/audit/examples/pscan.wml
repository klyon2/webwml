#use wml::debian::template title="Exemplo de auditoria automatizada: pscan"
#use wml::debian::recent_list
#use wml::debian::translation-check translation="be191e77facf8c0d489cfd320232517e5233a3e2"

<p>O <a href="https://packages.debian.org/pscan">pscan</a> é um pacote projetado
para auditar arquivos fonte C e C++ em busca por vulnerabilidades de
formato de string.</p>
<p>Não é uma ferramenta de auditoria de uso geral.</p>

<h2>Executando o pscan</h2>
<p>Executar pscan é uma simples questão de executá-lo com o nome de um arquivo,
ou arquivos, para verificar. Por exemplo:</p>
<pre>
pscan <a href="test.c.html">test.c</a>
</pre>
<p>A saída será escrita diretamente no console:</p>
<hr />
<samp>
test.c:42 SECURITY: printf call should have "%s" as argument 0
</samp>
<hr />

<h2>A saída</h2>
<p>A saída neste caso é fácil de entender. É corretamente identificado o
 fato que a chamada <tt>printf</tt> não responde seus
 argumentos de forma apropriada.</p>
<p>A saída também nos mostra o que devemos fazer para corrigir a falha,
altere o código que lê:
<pre>
printf( buff );
</pre>
<p>para ler:</p>
<pre>
printf( "%s", buff );
</pre>
<p>Não fazer isso poderia permitir que um(a) invasor(a), que possa controlar a
saída de <tt>ls</tt>, atacar o programa, criando um arquivo chamado "%s", ou
semelhante.</p>
<p>Ataques de formato de string são discutidos
<a href="http://www.securityfocus.com/guest/3342">nesta introdução do foco
de segurança</a>.</p>
<p>O <a href="http://www.dwheeler.com/secure-programs/">HOWTO de programação
segurança para Linux e Unix</a> explica como se proteger contra esses ataques
em funções variáveis normalmente usadas, como:</p>
<ul>
<li>printf</li>
<li>fprintf</li>
<li>syslog</li>
</ul>
<hr />
<p><a href="..">Voltar para o projeto de auditoria</a>
<a href="index">Voltar para a página de exemplos de auditoria</a></p>
