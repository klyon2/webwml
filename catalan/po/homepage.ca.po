# homepage webwml Catalan template.
# Guillem Jover <guillem@debian.org>, 2020.
#
msgid ""
msgstr ""
"Project-Id-Version: webwml\n"
"PO-Revision-Date: 2020-12-20 23:38+0100\n"
"Last-Translator: Guillem Jover <guillem@debian.org>\n"
"Language-Team: debian-l10n-catalan@lists.debian.org\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/index.def:8
msgid "The Universal Operating System"
msgstr "El sistema operatiu universal"

#: ../../english/index.def:12
msgid "DC19 Group Photo"
msgstr "Foto de grup de DC19"

#: ../../english/index.def:15
msgid "DebConf19 Group Photo"
msgstr "Foto de grup de DebConf19"

#: ../../english/index.def:19
msgid "Mini DebConf Hamburg 2018"
msgstr "Mini DebConf Hamburg 2018"

#: ../../english/index.def:22
msgid "Group photo of the MiniDebConf in Hamburg 2018"
msgstr "Foto de grup de la MiniDebConf a Hamburg 2018"

#: ../../english/index.def:26
msgid "Screenshot Calamares Installer"
msgstr "Captura de pantalla de l'instal·lador Calamares"

#: ../../english/index.def:29
msgid "Screenshot from the Calamares installer"
msgstr "Captura de pantalla de l'instal·lador Calamares"

#: ../../english/index.def:33 ../../english/index.def:36
msgid "Debian is like a Swiss Army Knife"
msgstr "Debian és com una navalla suïssa"

#: ../../english/index.def:40
msgid "People have fun with Debian"
msgstr "La gent es diverteix amb Debian"

#: ../../english/index.def:43
msgid "Debian people at Debconf18 in Hsinchu really having fun"
msgstr "Gent de Debian passant-ho bé a la DebConf18 a Hsinchu"

#: ../../english/template/debian/navbar.wml:31
msgid "Bits from Debian"
msgstr "«Bits from Debian», el bloc de Debian"

#: ../../english/template/debian/navbar.wml:31
msgid "Blog"
msgstr "Bloc"

#: ../../english/template/debian/navbar.wml:32
msgid "Micronews"
msgstr "Micronotícies"

#: ../../english/template/debian/navbar.wml:32
msgid "Micronews from Debian"
msgstr "Micronotícies de Debian"

#: ../../english/template/debian/navbar.wml:33
msgid "Planet"
msgstr "Planeta"

#: ../../english/template/debian/navbar.wml:33
msgid "The Planet of Debian"
msgstr "El planeta de Debian"
