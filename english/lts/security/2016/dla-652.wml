<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities have been found in QEMU:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7161">CVE-2016-7161</a>

    <p>Heap-based buffer overflow in the .receive callback of xlnx.xps-ethernetlite
    in QEMU (aka Quick Emulator) allows attackers to execute arbitrary code on
    the QEMU host via a large ethlite packet.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7170">CVE-2016-7170</a>

    <p>The vmsvga_fifo_run function in hw/display/vmware_vga.c in QEMU (aka Quick
    Emulator) is vulnerable to an OOB memory access.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7908">CVE-2016-7908</a>

    <p>The mcf_fec_do_tx function in hw/net/mcf_fec.c in QEMU (aka Quick Emulator)
    does not properly limit the buffer descriptor count when transmitting
    packets, which allows local guest OS administrators to cause a denial of
    service (infinite loop and QEMU process crash) via vectors involving a
    buffer descriptor with a length of 0 and crafted values in bd.flags.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.1.2+dfsg-6+deb7u16.</p>

<p>We recommend that you upgrade your qemu packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-652.data"
# $Id: $
