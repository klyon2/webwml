<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>In the recently uploaded systemd security update (215-17+deb8u12 via
DLA-1762-1), a regression was discovered in the fix for <a href="https://security-tracker.debian.org/tracker/CVE-2017-18078">CVE-2017-18078</a>.</p>

<p>The observation of Debian jessie LTS users was, that after upgrading to
+deb8u12 temporary files would not have the correct ownerships and
permissions anymore (instead of a file being owned by a specific user
and/or group, files were being owned by root:root; setting POSIX file
permissions (rwx, etc.) was also affected).</p>


<p>For Debian 8 <q>Jessie</q>, this regression problem has been fixed in version
215-17+deb8u13.</p>

<p>We recommend that you upgrade your systemd packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1762-2.data"
# $Id: $
