<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>A vulnerability was discovered by Lukas Kupczyk of the Advanced Research
Team at CrowdStrike Intelligence in OpenConnect, an open client for
Cisco AnyConnect, Pulse, GlobalProtect VPN.  A malicious HTTP server
(after its identity certificate has been accepted) can provide bogus
chunk lengths for chunked HTTP encoding and cause a heap overflow.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
6.00-2+deb8u1.</p>

<p>We recommend that you upgrade your openconnect packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1945.data"
# $Id: $
