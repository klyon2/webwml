<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>This update includes the changes in tzdata 2018c. Notable
changes are:</p>

 <p>- Brazil has canceled DST and will stay on standard time indefinitely.
 - Fiji's next DST transitions will be 2019-11-10 and 2020-01-12
   instead of 2019-11-03 and 2020-01-19.
 - Norfolk Island will observe Australian-style DST starting in
   spring 2019. The first transition is on 2019-10-06.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
2019c-0+deb8u1.</p>

<p>We recommend that you upgrade your tzdata packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1957.data"
# $Id: $
