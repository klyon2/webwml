<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several vulnerabilities in wordpress, a web blogging tool, have been fixed.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-17669">CVE-2019-17669</a>

    <p>Server Side Request Forgery (SSRF) vulnerability because URL
    validation does not consider the interpretation of a name as a
    series of hex characters.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-17670">CVE-2019-17670</a>

   <p>Server Side Request Forgery (SSRF) vulnerability was reported in
   wp_validate_redirect(). Normalize the path when validating the
   location for relative URLs.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-17671">CVE-2019-17671</a>

   <p>Unauthenticated viewing of certain content (private or draft posts)
   is possible because the static query property is mishandled.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-17675">CVE-2019-17675</a>

    <p>Wordpress does not properly consider type confusion during
    validation of the referer in the admin pages. This vulnerability
    affects the check_admin_referer() WordPress function.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
4.1.28+dfsg-0+deb8u1.</p>

<p>We recommend that you upgrade your wordpress packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1980.data"
# $Id: $
