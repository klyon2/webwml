<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A XSS vulnerability was discovered in SquirrelMail.  Due to improper
handling of RCDATA and RAWTEXT type elements, the built-in
sanitization mechanism can be bypassed.  Malicious script content from
HTML e-mails can be executed within the application context via
crafted use of (for example) a NOEMBED, NOFRAMES, NOSCRIPT, or
TEXTAREA element.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
2:1.4.23~svn20120406-2+deb8u4.</p>

<p>We recommend that you upgrade your squirrelmail packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1868.data"
# $Id: $
