<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were discovered in BIND, a DNS server
implementation.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-25214">CVE-2021-25214</a>

    <p>Greg Kuechle discovered that a malformed incoming IXFR transfer
    could trigger an assertion failure in named, resulting in denial
    of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-25215">CVE-2021-25215</a>

    <p>Siva Kakarla discovered that named could crash when a DNAME record
    placed in the ANSWER section during DNAME chasing turned out to be
    the final answer to a client query.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-25216">CVE-2021-25216</a>

    <p>It was discovered that the SPNEGO implementation used by BIND is
    prone to a buffer overflow vulnerability. This update switches to
    use the SPNEGO implementation from the Kerberos libraries.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
1:9.10.3.dfsg.P4-12.3+deb9u9.</p>

<p>We recommend that you upgrade your bind9 packages.</p>

<p>For the detailed security status of bind9 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/bind9">https://security-tracker.debian.org/tracker/bind9</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2647.data"
# $Id: $
