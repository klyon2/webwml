<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Imre Rad discovered several vulnerabilities in GNU patch, leading to
shell command injection or escape from the working directory and access
and overwrite files, if specially crafted patch files are processed.</p>

<p>This update includes a bugfix for a regression introduced by the patch
to address <a href="https://security-tracker.debian.org/tracker/CVE-2018-1000156">CVE-2018-1000156</a> when applying an ed-style patch (#933140).</p>

<p>For the oldstable distribution (stretch), these problems have been fixed
in version 2.7.5-1+deb9u2.</p>

<p>For the stable distribution (buster), these problems have been fixed in
version 2.7.6-3+deb10u1.</p>

<p>We recommend that you upgrade your patch packages.</p>

<p>For the detailed security status of patch please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/patch">https://security-tracker.debian.org/tracker/patch</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4489.data"
# $Id: $
